import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.feature_selection import mutual_info_regression

def get_dataset():
    all_solar_dataframe = pd.read_csv('Solar_data/Task 15/predictors15.csv', sep=',')

    dataframe_1 = all_solar_dataframe.loc[all_solar_dataframe['ZONEID'] == 1]
    dataframe_2 = all_solar_dataframe.loc[all_solar_dataframe['ZONEID'] == 2]
    dataframe_3 = all_solar_dataframe.loc[all_solar_dataframe['ZONEID'] == 3]
    solar_dataframe = dataframe_3

    return solar_dataframe

def update_dataset():

    solar_dataframe =get_dataset()

    # new data frame with split value columns
    new = solar_dataframe["TIMESTAMP"].str.split(" ", n=1, expand=True)

    # making seperate first name column from new data frame
    solar_dataframe["Date"] = new[0]

    # making seperate last name column from new data frame
    solar_dataframe["Hour"] = new[1]

    # Dropping old Name columns
    solar_dataframe.drop(columns=["TIMESTAMP"], inplace=True)

    return solar_dataframe

def data_selection():

    solar_dataframe = update_dataset()

    # Convert Date values to numeric values
    cols = ['Date']
    solar_dataframe[cols] = solar_dataframe[cols].apply(pd.to_numeric, errors='coerce', axis=1)


    date_index_object = solar_dataframe.iloc[:, 14].values.reshape(-1,1)
    hour_index = solar_dataframe.iloc[:, 15].str[0:2].values.reshape(-1,1)
    all_features = solar_dataframe.iloc[:, 1:13].values

    # Stack all the features
    X = np.hstack((date_index_object, hour_index, all_features))
    # target variable
    y = solar_dataframe.iloc[:, 13].values.reshape(-1,1)

    return X, y

def mutual_info_regr(X, y):
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}


    N = 15
    ind = np.arange(N)

    fig_loss = plt.figure()
    ax = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax.get_xticklabels() + ax.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(18)

    xc = np.arange(X.shape[1])
    ax.bar(xc, mutual_info_regression(X, y))
    ax.set_xlabel('All Features', **axis_font)
    ax.set_ylabel('Mutual Info Regression Score', **axis_font)
    ax.set_title('Mutual Information (Zone 3)', **title_font)
    plt.xticks(ind, ('DATE', 'HOUR', 'VAR78', 'VAR79', 'VAR134', 'VAR157', 'VAR164', 'VAR165', 'VAR166', 'VAR167',
                     'VAR169', 'VAR175', 'VAR178', 'VAR228'))
    ax.legend(['All Features'])

    plt.show()


def main():

    X, y = data_selection()
    mutual_info_regr(X, y)






if __name__=='__main__':

    main()