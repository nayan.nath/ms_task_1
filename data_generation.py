import numpy as np
import pandas as pd


def get_dataset():
    all_solar_dataframe = pd.read_csv('Solar_data/Task 15/predictors15.csv', sep=',')

    dataframe_1 = all_solar_dataframe.loc[all_solar_dataframe['ZONEID'] == 1]
    dataframe_2 = all_solar_dataframe.loc[all_solar_dataframe['ZONEID'] == 2]
    dataframe_3 = all_solar_dataframe.loc[all_solar_dataframe['ZONEID'] == 3]

    # Select the data ZONE
    solar_dataframe = dataframe_3

    return solar_dataframe


def data_generation_new():
    raw_data = get_dataset()

    #Zone 3 raw data
    solar_dataframe = pd.DataFrame(raw_data,
                                   columns=['ZONEID', 'TIMESTAMP', 'VAR157', 'VAR167', 'VAR169', 'VAR175', 'VAR178',
                                            'POWER'])


    #Calculate the average value of VAR175 and VAR178
    average_175 = solar_dataframe['VAR175'].mean()
    average_178 = solar_dataframe['VAR178'].mean()

    # Data Greater or equal than average value'''
    condition_for_feature_col_175 = solar_dataframe['VAR175'] >= average_175
    condition_for_feature_col_178 = solar_dataframe['VAR178'] >= average_178
    solar_dataframe_1 = solar_dataframe[condition_for_feature_col_175 & condition_for_feature_col_178]

    # Data less than average value
    condition_for_feature_col_175 = solar_dataframe['VAR175'] <= average_175
    condition_for_feature_col_178 = solar_dataframe['VAR178'] <= average_178
    solar_dataframe_2 = solar_dataframe[condition_for_feature_col_175 & condition_for_feature_col_178]

    # average value less or equal than VAR175 and greater or equal than VAR178
    condition_for_feature_col_175 = solar_dataframe['VAR175'] <= average_175
    condition_for_feature_col_178 = solar_dataframe['VAR178'] >= average_178
    solar_dataframe_3 = solar_dataframe[condition_for_feature_col_175 & condition_for_feature_col_178]

    # average value less or equal than VAR178 and greater or equal than VAR175
    condition_for_feature_col_175 = solar_dataframe['VAR175'] >= average_175
    condition_for_feature_col_178 = solar_dataframe['VAR178'] <= average_178
    solar_dataframe_4 = solar_dataframe[condition_for_feature_col_175 & condition_for_feature_col_178]

    return solar_dataframe_1, solar_dataframe_2, solar_dataframe_3, solar_dataframe_4


def main():

    solar_dataframe_1, solar_dataframe_2, solar_dataframe_3, solar_dataframe_4 = data_generation_new()

    print(solar_dataframe_1.columns)

    # np.savetxt('Zone_3_raw_data.csv', data, delimiter=',', fmt='%s')

    np.savetxt('processed_data_gtr_than_avg.csv', solar_dataframe_1, delimiter=',', fmt='%s')

    np.savetxt('processed_data_less_than_avg.csv', solar_dataframe_2, delimiter=',', fmt='%s')

    np.savetxt('processed_data_less_VAR175_greater_VAR178.csv', solar_dataframe_3, delimiter=',', fmt='%s')

    np.savetxt('processed_data_greater_VAR178_less_VAR175.csv', solar_dataframe_4, delimiter=',', fmt='%s')


if __name__ == "__main__":
    main()