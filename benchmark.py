import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense, Dropout
import keras
from sklearn.svm import SVR as regr
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score, explained_variance_score


def get_dataset(data):
    """
    solar_dataframe_1 = pd.read_csv('processed_data_gtr_than_avg.csv', sep=',')
    solar_dataframe_2 = pd.read_csv('processed_data_less_than_avg.csv', sep=',')
    solar_dataframe_3 = pd.read_csv('processed_data_less_VAR175_greater_VAR178.csv', sep=',')
    solar_dataframe_4 = pd.read_csv('processed_data_greater_VAR178_less_VAR175.csv', sep=',')
    """

    solar_dataframe = data

    return solar_dataframe

def update_dataset(solar_dataframe):
    # solar_dataframe =get_dataset(X)

    # new data frame with split value columns
    new = solar_dataframe["TIMESTAMP"].str.split(" ", n=1, expand=True)

    # making separate first name column from new data frame
    solar_dataframe["Date"] = new[0]

    # making separate last name column from new data frame
    solar_dataframe["Hour"] = new[1]

    # Dropping old Name columns
    solar_dataframe.drop(columns=["TIMESTAMP"], inplace=True)

    return solar_dataframe

def data_selection(X):
    solar_dataframe = update_dataset(X)

    # Convert Date values to numeric values
    cols = ['Date']
    solar_dataframe[cols] = solar_dataframe[cols].apply(pd.to_numeric, errors='coerce', axis=1)

    ZONEID = solar_dataframe.iloc[:, 0].values.reshape(-1, 1)
    VAR157 = solar_dataframe.iloc[:, 1].values.reshape(-1, 1)
    VAR167 = solar_dataframe.iloc[:, 2].values.reshape(-1, 1)
    VAR169 = solar_dataframe.iloc[:, 3].values.reshape(-1, 1)
    VAR175 = solar_dataframe.iloc[:, 4].values.reshape(-1, 1)
    VAR178 = solar_dataframe.iloc[:, 5].values.reshape(-1, 1)
    DATE = solar_dataframe.iloc[:, 7].values.reshape(-1, 1)
    HOUR = solar_dataframe.iloc[:, 8].str[0:2].values.reshape(-1, 1)

    POWER = solar_dataframe.iloc[:, 6].values.reshape(-1, 1)

    # Select all the features for input
    solar_X = np.hstack((VAR157, VAR167, VAR169, VAR175, VAR178))
    # target varable
    solar_y = POWER

    return solar_X, solar_y

def scaling_operation(do_scaling, X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)

    if do_scaling:

        scaler_x_minmax = MinMaxScaler()

        scaler_x_minmax.fit(X_train)
        X_train_scaled = scaler_x_minmax.transform(X_train)

        X_test_scaled = scaler_x_minmax.transform(X_test)

        scaler_y_minmax = MinMaxScaler()
        scaler_y_minmax.fit(y_train)
        y_train_scaled = scaler_y_minmax.transform(y_train)

        return scaler_y_minmax, X_train_scaled, X_test_scaled, y_train_scaled, y_test, scaler_x_minmax

    else:

        return None, X_train, X_test, y_train, y_test, None

def train_model(X, y, use_keras=False, params=None):

    if params == None:

        num_layers = 2
        num_neurons = 130
        activation = 'relu'
        learning_rate_init = 1e-4
        n_epochs = 10
        batch_size = 100
        dropout = Dropout(0)

    else:
        num_layers = params['num_layers']
        num_neurons = params['num_neurons']
        activation = params['activation']
        learning_rate_init = params['learning_rate_init']
        n_epochs = params['n_epochs']
        batch_size = params['batch_size']
        dropout = params['dropout']

    if use_keras:
        keras.backend.clear_session()

        # Select an Optimizer
        optimizer = keras.optimizers.Adam(lr=learning_rate_init)

        # Initialize a sequential/ feed forward model
        regressor = Sequential()

        # Add input and first hidden layer
        regressor.add(Dense(units=num_neurons, activation=activation, input_dim=X.shape[1]))

        # add dropout
        regressor.add(dropout)

        # Add subsequent hidden layer
        for _ in range(num_layers - 1):
            regressor.add(Dense(units=num_neurons,
                                activation=activation
                                )
                          )

        # Add Output Layer
        regressor.add(Dense(units=y.shape[1], activation='relu'))

        # Compile the regressor
        regressor.compile(optimizer=optimizer, loss='mse', metrics=['accuracy'])

        # Set up for validation during the training
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

        # Train the model, varbose = 1(Print the result in the screen during the training)
        # history = regressor.fit(X, y, epochs=n_epochs, batch_size=batch_size, validation_data=(X_test, y_test),
        #                       verbose=0)

        history = regressor.fit(X, y, validation_split=0.20, epochs=n_epochs, batch_size=batch_size, verbose=1)

        # Set the font dictionaries (for plot title and axis titles)
        title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                      'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
        axis_font = {'fontname': 'Arial', 'size': '24'}

        fig_loss = plt.figure()
        ax_loss = fig_loss.add_subplot(1, 1, 1)

        # Set the tick labels font
        for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
            label.set_fontname('Arial')
            label.set_fontsize(24)

        """
        # summarize history for accuracy
        plt.plot(history.history['acc'])
        plt.plot(history.history['val_acc'])
        plt.title('model accuracy', **title_font)
        plt.ylabel('accuracy', **axis_font)
        plt.xlabel('epoch', **axis_font)
        plt.legend(['train', 'test'], loc='upper left')
        plt.show()
        """

        # summarize history for loss
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('model loss', **title_font)
        plt.ylabel('loss', **axis_font)
        plt.xlabel('epoch', **axis_font)
        plt.legend(['train', 'validation'], loc='upper left')
        plt.show()


    else:

        regressor = regr(kernel='rbf', gamma='scale',
                         C=1.0, epsilon=0.01,
                         shrinking=True, tol=0.001)
        regressor.fit(X, y)

    return regressor

def get_metrics(true, predicted):
    error = true - predicted
    pe = (error) / true

    mae = mean_absolute_error(true, predicted)  # np.mean(np.abs(error))
    print('Mean Absolute Error : {}'.format(mae))

    rmse = np.sqrt(mean_squared_error(true, predicted))
    print('Root Mean Squared Error : {}'.format(rmse))

    evs = explained_variance_score(true, predicted)
    print('Explained Variance Score: {}'.format(evs))

    r2_s = r2_score(y_true=true, y_pred=predicted)
    print('R2 Score : {}'.format(r2_s))

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    plt.hist(error, 50)
    # plt.xlim(-50,50, 0, 0.3)
    plt.xlabel('Variance', **axis_font)
    plt.ylabel('Number of Observations', **axis_font)
    plt.title('Histogram of Error', **title_font)
    # plt.grid()
    plt.show()

    return mae, rmse, evs, r2_s

def plot_results(true, predicted, scores):
    """
    :param true: Give your original data
    :param predicted: Give your Predicted data
    :return: Predicted result plot
    """

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    ax_loss.plot(true)
    plt.plot(predicted)
    ax_loss.set_title('Zone 3 Predicted Result \n' + " MAE:" + str(round(scores[1], 3)) + " RMSE:" + str(
        round(scores[2], 3)) + " EVS:" + str(round(scores[3], 3)), **title_font)
    ax_loss.set_ylabel('Predicted Power', **axis_font)
    ax_loss.set_xlabel('Number of Observations', **axis_font)

    ax_loss.grid()
    plt.legend(('Actual', 'Predicted'), loc='upper right')
    plt.show()

def main():


    #############################################################################################################################
    ''' Solar Dataframe 1   '''
    #############################################################################################################################

    solar_dataframe1 = get_dataset(pd.read_csv('processed_data_gtr_than_avg.csv', sep=','))
    scaling = True
    solar_X, solar_y = data_selection(solar_dataframe1)

    scaler_y_minmax, X_train_scaled, X_test_scaled, y_train_scaled, y_test, scaler_x_minmax = scaling_operation(scaling,
                                                                                                                solar_X,
                                                                                                                solar_y)
    print('X_train_scaled: {}\ny_train_scaled: {}\nX_test_scaled: {}\ny_test : {}'.format(X_train_scaled.shape, y_train_scaled.shape, X_test_scaled.shape, y_test.shape))

    # Hyper parameter tuning for solar dataframe 1
    param = {}
    param['num_layers'] = 4
    param['num_neurons'] = 150
    param['activation'] = 'relu'
    param['learning_rate_init'] = 0.001
    param['n_epochs'] = 90
    param['batch_size'] = 1649
    param['dropout'] = Dropout(0.3)

    model = train_model(X_train_scaled, y_train_scaled,
                        use_keras = False,
                        params=param)

    if scaling:
        # Predict Power with scaled data
        predicted_scaled = model.predict(X_test_scaled)
        # Remove Scaling after prediction
        predicted_actual = scaler_y_minmax.inverse_transform(
            predicted_scaled.reshape(-1, 1))

    else:
        # Predict Power without scaled data
        predicted_actual = model.predict(X_test_scaled)

    print('\n\nMetrics for Neural Net')
    scr = get_metrics(y_test, predicted_actual)
    plot_results(y_test[0:23], predicted_actual[0:23], scr)



    #############################################################################################################################
    ''' Solar Dataframe 2   '''
    #############################################################################################################################

    solar_dataframe2 = get_dataset(pd.read_csv('processed_data_less_than_avg.csv', sep=','))
    scaling = True
    solar_X, solar_y = data_selection(solar_dataframe2)

    scaler_y_minmax, X_train_scaled, X_test_scaled, y_train_scaled, y_test, scaler_x_minmax = scaling_operation(scaling,
                                                                                                                solar_X,
                                                                                                                solar_y)
    print('X_train_scaled: {}\ny_train_scaled: {}\nX_test_scaled: {}\ny_test : {}'.format(X_train_scaled.shape,
                                                                                          y_train_scaled.shape,
                                                                                          X_test_scaled.shape,
                                                                                          y_test.shape))
    # Hyper parameter tuning for solar dataframe 2
    param = {}
    param['num_layers'] = 2
    param['num_neurons'] = 130
    param['activation'] = 'relu'
    param['learning_rate_init'] = 0.001
    param['n_epochs'] = 40
    param['batch_size'] = 100
    param['dropout'] = Dropout(0.3)

    model = train_model(X_train_scaled, y_train_scaled,
                        use_keras=False,
                        params=param)

    if scaling:
        # Predict Power with scaled data
        predicted_scaled = model.predict(X_test_scaled)
        # Remove Scaling after prediction
        predicted_actual = scaler_y_minmax.inverse_transform(
            predicted_scaled.reshape(-1, 1))

    else:
        # Predict Power without scaled data
        predicted_actual = model.predict(X_test_scaled)

    print('\n\nMetrics for Neural Net')
    scr = get_metrics(y_test, predicted_actual)
    plot_results(y_test[0:23], predicted_actual[0:23], scr)


    #############################################################################################################################
    ''' Solar Dataframe 3   '''
    #############################################################################################################################

    solar_dataframe3 = get_dataset(pd.read_csv('processed_data_less_VAR175_greater_VAR178.csv', sep=','))
    scaling = True
    solar_X, solar_y = data_selection(solar_dataframe3)
    scaler_y_minmax, X_train_scaled, X_test_scaled, y_train_scaled, y_test, scaler_x_minmax = scaling_operation(scaling,
                                                                                                                solar_X,
                                                                                                                solar_y)
    print('X_train_scaled: {}\ny_train_scaled: {}\nX_test_scaled: {}\ny_test : {}'.format(X_train_scaled.shape,
                                                                                          y_train_scaled.shape,
                                                                                          X_test_scaled.shape,
                                                                                          y_test.shape))

    # Hyper parameter tuning for solar dataframe 3
    param = {}
    param['num_layers'] = 2
    param['num_neurons'] = 130
    param['activation'] = 'relu'
    param['learning_rate_init'] = 0.001
    param['n_epochs'] = 50
    param['batch_size'] = 100
    param['dropout'] = Dropout(0.3)
    model = train_model(X_train_scaled, y_train_scaled,
                        use_keras=False,
                        params=param)

    if scaling:
        # Predict Power with scaled data
        predicted_scaled = model.predict(X_test_scaled)
        # Remove Scaling after prediction
        predicted_actual = scaler_y_minmax.inverse_transform(
            predicted_scaled.reshape(-1, 1))

    else:
        # Predict Power without scaled data
        predicted_actual = model.predict(X_test_scaled)

    print('\n\nMetrics for Neural Net')
    scr = get_metrics(y_test, predicted_actual)
    plot_results(y_test[0:23], predicted_actual[0:23], scr)



    #############################################################################################################################
    ''' Solar Dataframe 4   '''
    #############################################################################################################################

    solar_dataframe4 = get_dataset(pd.read_csv('processed_data_greater_VAR178_less_VAR175.csv', sep=','))
    scaling = True
    solar_X, solar_y = data_selection(solar_dataframe4)

    scaler_y_minmax, X_train_scaled, X_test_scaled, y_train_scaled, y_test, scaler_x_minmax = scaling_operation(scaling,
                                                                                                                solar_X,
                                                                                                                solar_y)

    print('X_train_scaled: {}\ny_train_scaled: {}\nX_test_scaled: {}\ny_test : {}'.format(X_train_scaled.shape,
                                                                                          y_train_scaled.shape,
                                                                                          X_test_scaled.shape,
                                                                                          y_test.shape))

    # Hyper parameter tuning for solar dataframe 4
    param = {}
    param['num_layers'] = 5
    param['num_neurons'] = 200
    param['activation'] = 'relu'
    param['learning_rate_init'] = 0.001
    param['n_epochs'] = 50
    param['batch_size'] = 100
    param['dropout'] = Dropout(0.3)

    model = train_model(X_train_scaled, y_train_scaled,
                        use_keras=False,
                        params=param)

    if scaling:
        # Predict Power with scaled data
        predicted_scaled = model.predict(X_test_scaled)
        # Remove Scaling after prediction
        predicted_actual = scaler_y_minmax.inverse_transform(
            predicted_scaled.reshape(-1, 1))

    else:
        # Predict Power without scaled data
        predicted_actual = model.predict(X_test_scaled)

    print('\n\nMetrics for Neural Net')
    scr = get_metrics(y_test, predicted_actual)
    plot_results(y_test[0:23], predicted_actual[0:23], scr)



if __name__ == '__main__':
    main()