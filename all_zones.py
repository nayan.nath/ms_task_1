import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense
import keras
from sklearn.neighbors import KNeighborsRegressor as regr
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score, explained_variance_score
from sklearn.preprocessing import LabelEncoder



def get_dataset():
    all_solar_dataframe = pd.read_csv('Solar_data/Task 15/predictors15.csv', sep=',')

    dataframe_1 = all_solar_dataframe.loc[all_solar_dataframe['ZONEID'] == 1]
    dataframe_2 = all_solar_dataframe.loc[all_solar_dataframe['ZONEID'] == 2]
    dataframe_3 = all_solar_dataframe.loc[all_solar_dataframe['ZONEID'] == 3]

    solar_dataframe = [dataframe_1, dataframe_2, dataframe_3]
    solar_dataframe = pd.concat(solar_dataframe)

    return solar_dataframe

def update_dataset():

    solar_dataframe =get_dataset()

    # new data frame with split value columns
    new = solar_dataframe["TIMESTAMP"].str.split(" ", n=1, expand=True)

    # making separate first name column from new data frame
    solar_dataframe["Date"] = new[0]

    # making separate last name column from new data frame
    solar_dataframe["Hour"] = new[1]

    # Dropping old Name columns
    solar_dataframe.drop(columns=["TIMESTAMP"], inplace=True)

    return solar_dataframe

def one_hot_encoding():

    solar_dataframe = update_dataset()

    # Select all the Zone
    zone_id = solar_dataframe.iloc[:, 0].values

    # integer encode
    label_encoder = LabelEncoder()
    integer_encoded = label_encoder.fit_transform(zone_id)

    # binary encode
    onehot_encoder = OneHotEncoder(sparse=False)
    integer_encoded = integer_encoded.reshape(len(integer_encoded), 1)
    zone_onehot_encoded = onehot_encoder.fit_transform(integer_encoded)

    return zone_onehot_encoded


def data_selection():

    solar_dataframe = update_dataset()

    # Get all the zone id as one hot encoder
    zone_onehot_encoded = one_hot_encoding()

    # Convert Date values to numeric values
    cols = ['Date']
    solar_dataframe[cols] = solar_dataframe[cols].apply(pd.to_numeric, errors='coerce', axis=1)

    date_index_object = solar_dataframe.iloc[:, 14].values.reshape(-1, 1)
    hour_index = solar_dataframe.iloc[:, 15].str[0:2].values.reshape(-1, 1)
    var_157 = solar_dataframe.iloc[:, 4].values.reshape(-1, 1)
    var_167 = solar_dataframe.iloc[:, 8].values.reshape(-1, 1)
    var_169 = solar_dataframe.iloc[:, 9].values.reshape(-1, 1)
    var_175 = solar_dataframe.iloc[:, 10].values.reshape(-1, 1)
    var_178 = solar_dataframe.iloc[:, 11].values.reshape(-1, 1)

    # Stack all the features
    solar_X = np.hstack((date_index_object, zone_onehot_encoded, hour_index, var_157, var_167, var_169, var_175, var_178))

    # target variable
    solar_y = solar_dataframe.iloc[:, 13].values.reshape(-1, 1)

    return solar_X, solar_y



def scaling_operation(do_scaling, X, y):

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)

    if do_scaling:

        scaler_x_minmax = MinMaxScaler()

        scaler_x_minmax.fit(X_train)
        X_train_scaled = scaler_x_minmax.transform(X_train)

        X_test_scaled = scaler_x_minmax.transform(X_test)

        scaler_y_minmax = MinMaxScaler()
        scaler_y_minmax.fit(y_train)
        y_train_scaled = scaler_y_minmax.transform(y_train)

        return scaler_y_minmax, X_train_scaled, X_test_scaled, y_train_scaled, y_test, scaler_x_minmax

    else:

        return None, X_train, X_test, y_train, y_test, None

def train_model(X, y, use_keras=False, params=None):

    if params == None:

        num_layers = 2
        num_neurons = 130
        activation = 'relu'
        learning_rate_init = 1e-4
        n_epochs = 10
        batch_size = 100

    else:
        num_layers = params['num_layers']
        num_neurons = params['num_neurons']
        activation = params['activation']
        learning_rate_init = params['learning_rate_init']
        n_epochs = params['n_epochs']
        batch_size = params['batch_size']

    if use_keras:
        keras.backend.clear_session()

        # Select an Optimizer
        optimizer = keras.optimizers.Adam(lr=learning_rate_init)

        # Initialize a sequential/ feed forward model
        regressor = Sequential()

        # Add input and first hidden layer
        regressor.add(Dense(units=num_neurons, activation=activation, input_dim=X.shape[1]))

        # Add subsequent hidden layer
        for _ in range(num_layers - 1):
            regressor.add(Dense(units=num_neurons,
                                activation=activation
                                )
                          )

        # Add Output Layer
        regressor.add(Dense(units=y.shape[1], activation='relu'))

        # Complile the regressor
        regressor.compile(optimizer=optimizer, loss='mse', metrics=['accuracy'])

        # Set up for validation during the training
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

        # Train the model, varbose = 1(Print the result in the screen during the training)
        history = regressor.fit(X, y, epochs=n_epochs, batch_size=batch_size, validation_data=(X_test, y_test),
                                verbose=0)

        # Plot the results
        # Set the font dictionaries (for plot title and axis titles)
        title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                      'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
        axis_font = {'fontname': 'Arial', 'size': '24'}

        fig_loss = plt.figure()
        ax_loss = fig_loss.add_subplot(1, 1, 1)

        # Set the tick labels font
        for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
            label.set_fontname('Arial')
            label.set_fontsize(24)

        ax_loss.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        ax_loss.set_title('num_layers:' + str(num_layers) + ',num_neurons:' + str(num_neurons) +
                          ', activation:' + str(activation) + ', learning_rate_init:' + str(learning_rate_init) +
                          ', batch_size:' + str(batch_size), **title_font)
        ax_loss.set_ylabel('Loss(MAE)', **axis_font)
        ax_loss.set_xlabel('Number of Epochs', **axis_font)
        # ax_loss.grid()
        plt.show()



    else:

        regressor = regr(n_neighbors=100)
        regressor.fit(X, y)
        """
        regressor = regr()
        regressor = BaggingRegressor(regressor, n_estimators=5)

        for _ in range(n_epochs):
            regressor.fit(X, y)
        """

    return regressor

def get_metrics(true, predicted):

    error = true - predicted
    pe = (error) / true

    mae = mean_absolute_error(true, predicted)  # np.mean(np.abs(error))
    print('Mean Absolute Error : {}'.format(mae))

    rmse = np.sqrt(mean_squared_error(true, predicted))
    print('Root Mean Squared Error : {}'.format(rmse))

    evs = explained_variance_score(true, predicted)
    print('Explained Variance Score: {}'.format(evs))

    r2_s = r2_score(y_true=true, y_pred=predicted)
    print('R2 Score : {}'.format(r2_s))

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)


    plt.hist(error,50)
    #plt.xlim(-50,50, 0, 0.3)
    plt.xlabel('Variance', **axis_font)
    plt.ylabel('Number of Observations', **axis_font)
    plt.title('Histogram of Error', **title_font)
    #plt.grid()
    plt.show()

    return mae, rmse, evs, r2_s

def plot_results(true, predicted, scores):
    """

    :param true: Give your original data
    :param predicted: Give your Predicted data
    :return: Predicted result plot
    """
    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    ax_loss.plot(true)
    plt.plot(predicted)
    ax_loss.set_title(" MAE:" + str(round(scores[1], 3)) + " RMSE:" + str(
            round(scores[2], 3)) + " EVS:" + str(round(scores[3], 3)), **title_font)
    ax_loss.set_ylabel('Predicted Power', **axis_font)
    ax_loss.set_xlabel('Number of Observations', **axis_font)
    ax_loss.grid()
    plt.legend(('Actual', 'Predicted'), loc='upper right')
    plt.show()


def main():

    scaling = True

    solar_X, solar_y = data_selection()

    scaler_y_minmax, X_train_scaled, X_test_scaled, y_train_scaled, y_test, scaler_x_minmax = scaling_operation(scaling, solar_X, solar_y)

    # Hyper parameter tuning for Classification Model
    param = {}
    param['num_layers'] = 2
    param['num_neurons'] = 130
    param['activation'] = 'relu'
    param['learning_rate_init'] = 1e-4
    param['n_epochs'] = 100
    param['batch_size'] = 300

    model = train_model(X_train_scaled, y_train_scaled,
                                              use_keras=False,
                                              params=param)

    if scaling:
        # Predict Power with scaled data
        predicted_scaled = model.predict(X_test_scaled)
        # Remove Scaling after prediction
        predicted_actual = scaler_y_minmax.inverse_transform(
            predicted_scaled.reshape(-1, 1))

    else:
        # Predict Power without scaled data
        predicted_actual = model.predict(X_test_scaled)


    print('\n\nMetrics for Calculator')
    scr = get_metrics(y_test, predicted_actual)
    plot_results(y_test, predicted_actual, scr)

if __name__=='__main__':

    main()