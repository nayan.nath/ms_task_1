import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense
from sklearn.metrics import mean_squared_error, mean_absolute_error
from keras.layers.recurrent import LSTM
from numpy import concatenate

scaler = MinMaxScaler(feature_range=(0, 1))

def get_dataset(data):
    """
    solar_dataframe_1 = pd.read_csv('processed_data_gtr_than_avg.csv', sep=',')
    solar_dataframe_2 = pd.read_csv('processed_data_less_than_avg.csv', sep=',')
    solar_dataframe_3 = pd.read_csv('processed_data_less_VAR175_greater_VAR178.csv', sep=',')
    solar_dataframe_4 = pd.read_csv('processed_data_greater_VAR178_less_VAR175.csv', sep=',')
    """

    solar_dataframe = data

    return solar_dataframe

def update_dataset(solar_dataframe):
    # solar_dataframe =get_dataset(X)

    # new data frame with split value columns
    new = solar_dataframe["TIMESTAMP"].str.split(" ", n=1, expand=True)

    # making separate first name column from new data frame
    solar_dataframe["Date"] = new[0]

    # making separate last name column from new data frame
    solar_dataframe["Hour"] = new[1]

    # Dropping old Name columns
    solar_dataframe.drop(columns=["TIMESTAMP"], inplace=True)

    return solar_dataframe

def data_selection(X):
    solar_dataframe = update_dataset(X)

    # Convert Date values to numeric values
    cols = ['Date']
    solar_dataframe[cols] = solar_dataframe[cols].apply(pd.to_numeric, errors='coerce', axis=1)

    ZONEID = solar_dataframe.iloc[:, 0].values.reshape(-1, 1)
    VAR157 = solar_dataframe.iloc[:, 1].values.reshape(-1, 1)
    VAR167 = solar_dataframe.iloc[:, 2].values.reshape(-1, 1)
    VAR169 = solar_dataframe.iloc[:, 3].values.reshape(-1, 1)
    VAR175 = solar_dataframe.iloc[:, 4].values.reshape(-1, 1)
    VAR178 = solar_dataframe.iloc[:, 5].values.reshape(-1, 1)
    DATE = solar_dataframe.iloc[:, 7].values.reshape(-1, 1)
    HOUR = solar_dataframe.iloc[:, 8].str[0:2].values.reshape(-1, 1)

    POWER = solar_dataframe.iloc[:, 6].values.reshape(-1, 1)

    # Select all the features for input
    #solar_X = np.hstack((VAR157, VAR167, VAR169, VAR175, VAR178))

    solar_X = np.hstack((HOUR, VAR157, VAR167, VAR169, VAR175, VAR178))

    # target varable
    solar_y = POWER

    return solar_X, solar_y

def LSTM_data_selection(do_scaling, X, y):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)

    if do_scaling:

        X_train_scaled = scaler.fit_transform(X_train)
        y_train_scaled = scaler.fit_transform(y_train)
        X_test_scaled = scaler.fit_transform(X_test)
        y_test_scaled = scaler.fit_transform(y_test)

        # reshape input to be 3D [samples, time-steps, features]
        X_train_scaled = X_train_scaled.reshape((X_train_scaled.shape[0], 1, X_train_scaled.shape[1]))
        X_test_scaled = X_test_scaled.reshape((X_test_scaled.shape[0], 1, X_test_scaled.shape[1]))

        return X_train_scaled, X_test_scaled, y_train_scaled, y_test_scaled

    else:

        # reshape input to be 3D [samples, time-steps, features]
        X_train = X_train.reshape((X_train.shape[0], 1, X_train.shape[1]))
        X_test = X_test.reshape((X_test.shape[0], 1, X_test.shape[1]))

        return X_train, X_test, y_train, y_test

def train_LSTM_Dataframe1(X, y, params=None):
    if params == None:

        num_of_units = params[50]
        activation = params['relu']
        loss = params['mae']
        optimizer = params['adam']
        n_epochs = params['n_epochs']
        batch_size = params['batch_size']

    else:
        num_of_units = params['num_of_units']
        activation = params['activation']
        loss = params['loss']
        optimizer = params['optimizer']
        n_epochs = params['n_epochs']
        batch_size = params['batch_size']

    # Train LSTM model
    # expected input data shape: (batch_size, timesteps, data_dim)


    model = Sequential()
    model.add(LSTM(num_of_units, return_sequences=True,
                   input_shape=(X.shape[1], X.shape[2])))
    model.add(LSTM(num_of_units, return_sequences=True))
    model.add(LSTM(num_of_units))
    model.add(Dense(1, activation=activation))


    model.compile(loss=loss, optimizer=optimizer)
    # fit network
    history = model.fit(X, y, epochs=n_epochs, batch_size=batch_size, validation_split=0.20, verbose=1,
                        shuffle=True)

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss \n(Sub-dataset 1)', **title_font)
    plt.ylabel('Loss (mae)', **axis_font)
    plt.xlabel('Number of epochs', **axis_font)
    plt.legend(['training', 'validation'], loc='upper right')
    plt.grid()
    plt.show()

    return model

def train_LSTM_Dataframe2(X, y, params=None):
    if params == None:

        num_of_units = params[50]
        activation = params['relu']
        loss = params['mae']
        optimizer = params['adam']
        n_epochs = params['n_epochs']
        batch_size = params['batch_size']

    else:
        num_of_units = params['num_of_units']
        activation = params['activation']
        loss = params['loss']
        optimizer = params['optimizer']
        n_epochs = params['n_epochs']
        batch_size = params['batch_size']



    model = Sequential()
    model.add(LSTM(num_of_units, return_sequences=True,
                   input_shape=(X.shape[1], X.shape[2])))
    model.add(LSTM(num_of_units, return_sequences=True))
    model.add(LSTM(num_of_units, return_sequences=True))
    model.add(LSTM(num_of_units))
    model.add(Dense(1, activation=activation))

    model.compile(loss=loss, optimizer=optimizer)
    # fit network
    history = model.fit(X, y, epochs=n_epochs, batch_size=batch_size, validation_split=0.20, verbose=1,
                        shuffle=True)

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss \n(Sub-dataset 2)', **title_font)
    plt.ylabel('Loss (mae)', **axis_font)
    plt.xlabel('Number of epochs', **axis_font)
    plt.legend(['training', 'validation'], loc='upper right')
    plt.grid()
    plt.show()

    return model

def train_LSTM_Dataframe3(X, y, params=None):
    if params == None:

        num_of_units = params[50]
        activation = params['relu']
        loss = params['mae']
        optimizer = params['adam']
        n_epochs = params['n_epochs']
        batch_size = params['batch_size']

    else:
        num_of_units = params['num_of_units']
        activation = params['activation']
        loss = params['loss']
        optimizer = params['optimizer']
        n_epochs = params['n_epochs']
        batch_size = params['batch_size']

    # Train LSTM model
    # expected input data shape: (batch_size, timesteps, data_dim)

    model = Sequential()
    model.add(LSTM(num_of_units, return_sequences=True,
                   input_shape=(X.shape[1], X.shape[2])))
    model.add(LSTM(num_of_units, return_sequences=True))
    model.add(LSTM(num_of_units, return_sequences=True))
    model.add(LSTM(num_of_units))
    # Output layer (Single value prediction)
    model.add(Dense(1, activation=activation))


    model.compile(loss=loss, optimizer=optimizer)
    # fit network
    history = model.fit(X, y, epochs=n_epochs, batch_size=batch_size, validation_split=0.20, verbose=1,
                        shuffle=True)

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss \n(Sub-dataset 3)', **title_font)
    plt.ylabel('Loss (mae)', **axis_font)
    plt.xlabel('Number of epochs', **axis_font)
    plt.legend(['training', 'validation'], loc='upper right')
    plt.grid()
    plt.show()

    return model

def train_LSTM_Dataframe4(X, y, params=None):
    if params == None:

        num_of_units = params[50]
        activation = params['relu']
        loss = params['mae']
        optimizer = params['adam']
        n_epochs = params['n_epochs']
        batch_size = params['batch_size']

    else:
        num_of_units = params['num_of_units']
        activation = params['activation']
        loss = params['loss']
        optimizer = params['optimizer']
        n_epochs = params['n_epochs']
        batch_size = params['batch_size']


    model = Sequential()
    model.add(LSTM(num_of_units, return_sequences=True,
                   input_shape=(X.shape[1], X.shape[2])))
    model.add(LSTM(num_of_units, return_sequences=True))
    model.add(LSTM(num_of_units, return_sequences=True))
    model.add(LSTM(num_of_units))
    model.add(Dense(1, activation=activation))

    model.compile(loss=loss, optimizer=optimizer)
    # fit network
    history = model.fit(X, y, epochs=n_epochs, batch_size=batch_size, validation_split=0.20, verbose=1,
                        shuffle=True)

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss \n(Sub-dataset 4)', **title_font)
    plt.ylabel('Loss (mae)', **axis_font)
    plt.xlabel('Number of epochs', **axis_font)
    plt.legend(['training', 'validation'], loc='upper right')
    plt.grid()
    plt.show()

    return model

def get_metrics_1(true, predicted):
    error = true - predicted
    pe = (error) / true

    mae = mean_absolute_error(true, predicted)  # np.mean(np.abs(error))
    print('Mean Absolute Error : {}'.format(mae))

    rmse = np.sqrt(mean_squared_error(true, predicted))
    print('Root Mean Squared Error : {}'.format(rmse))

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    plt.hist(error, 50)
    # plt.xlim(-50,50, 0, 0.3)
    plt.xlabel('Variance', **axis_font)
    plt.ylabel('Number of observations', **axis_font)
    plt.title('Histogram of error \n(Sub-dataset 1)', **title_font)
    plt.grid()
    plt.show()
    return mae, rmse

def get_metrics_2(true, predicted):
    error = true - predicted
    pe = (error) / true

    mae = mean_absolute_error(true, predicted)  # np.mean(np.abs(error))
    print('Mean Absolute Error : {}'.format(mae))

    rmse = np.sqrt(mean_squared_error(true, predicted))
    print('Root Mean Squared Error : {}'.format(rmse))

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    plt.hist(error, 50)
    # plt.xlim(-50,50, 0, 0.3)
    plt.xlabel('Variance', **axis_font)
    plt.ylabel('Number of observations', **axis_font)
    plt.title('Histogram of error \n(Sub-dataset 2)', **title_font)
    plt.grid()
    plt.show()
    return mae, rmse

def get_metrics_3(true, predicted):
    error = true - predicted
    pe = (error) / true

    mae = mean_absolute_error(true, predicted)  # np.mean(np.abs(error))
    print('Mean Absolute Error : {}'.format(mae))

    rmse = np.sqrt(mean_squared_error(true, predicted))
    print('Root Mean Squared Error : {}'.format(rmse))

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    plt.hist(error, 50)
    # plt.xlim(-50,50, 0, 0.3)
    plt.xlabel('Variance', **axis_font)
    plt.ylabel('Number of observations', **axis_font)
    plt.title('Histogram of error \n(Sub-dataset 3)', **title_font)
    plt.grid()
    plt.show()
    return mae, rmse

def get_metrics_4(true, predicted):
    error = true - predicted
    pe = (error) / true

    mae = mean_absolute_error(true, predicted)  # np.mean(np.abs(error))
    print('Mean Absolute Error : {}'.format(mae))

    rmse = np.sqrt(mean_squared_error(true, predicted))
    print('Root Mean Squared Error : {}'.format(rmse))

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    plt.hist(error, 50)
    # plt.xlim(-50,50, 0, 0.3)
    plt.xlabel('Variance', **axis_font)
    plt.ylabel('Number of observations', **axis_font)
    plt.title('Histogram of error \n(Sub-dataset 4)', **title_font)
    plt.grid()
    plt.show()
    return mae, rmse

def plot_results_1(true, predicted, scores):
    """
    :param true: Give your original data
    :param predicted: Give your Predicted data
    :return: Predicted result plot
    """

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    ax_loss.plot(true)
    plt.plot(predicted)
    ax_loss.set_title('Zone 3 solar power predicted result \n(Sub-dataset 1)', **title_font)
    ax_loss.set_ylabel('Predicted solar power', **axis_font)
    ax_loss.set_xlabel('Number of observations', **axis_font)

    ax_loss.grid()
    plt.legend(('actual', 'predicted'), loc='upper right')
    plt.show()

def plot_results_2(true, predicted, scores):
    """
    :param true: Give your original data
    :param predicted: Give your Predicted data
    :return: Predicted result plot
    """

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    ax_loss.plot(true)
    plt.plot(predicted)
    ax_loss.set_title('Zone 3 solar power predicted result \n(Sub-dataset 2)', **title_font)
    ax_loss.set_ylabel('Predicted solar power', **axis_font)
    ax_loss.set_xlabel('Number of observations', **axis_font)

    ax_loss.grid()
    plt.legend(('actual', 'predicted'), loc='upper right')
    plt.show()

def plot_results_3(true, predicted, scores):
    """
    :param true: Give your original data
    :param predicted: Give your Predicted data
    :return: Predicted result plot
    """

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    ax_loss.plot(true)
    plt.plot(predicted)
    ax_loss.set_title('Zone 3 solar power predicted result \n(Sub-dataset 3)', **title_font)
    ax_loss.set_ylabel('Predicted solar power', **axis_font)
    ax_loss.set_xlabel('Number of observations', **axis_font)

    ax_loss.grid()
    plt.legend(('actual', 'predicted'), loc='upper right')
    plt.show()

def plot_results_4(true, predicted, scores):
    """
    :param true: Give your original data
    :param predicted: Give your Predicted data
    :return: Predicted result plot
    """

    # Set the font dictionaries (for plot title and axis titles)
    title_font = {'fontname': 'Arial', 'size': '24', 'color': 'black', 'weight': 'normal',
                  'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
    axis_font = {'fontname': 'Arial', 'size': '24'}

    fig_loss = plt.figure()
    ax_loss = fig_loss.add_subplot(1, 1, 1)

    # Set the tick labels font
    for label in (ax_loss.get_xticklabels() + ax_loss.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(24)

    ax_loss.plot(true)
    plt.plot(predicted)
    ax_loss.set_title('Zone 3 solar power predicted result \n(Sub-dataset 4)', **title_font)
    ax_loss.set_ylabel('Predicted solar power', **axis_font)
    ax_loss.set_xlabel('Number of observations', **axis_font)

    ax_loss.grid()
    plt.legend(('actual', 'predicted'), loc='upper right')
    plt.show()

def main():


    #############################################################################################################################
    ''' Solar Sub-dataset 1   '''
    #############################################################################################################################
    solar_dataframe1 = get_dataset(pd.read_csv('processed_data_gtr_than_avg.csv', sep=',', names=['ZONEID', 'TIMESTAMP', 'VAR157', 'VAR167', 'VAR169', 'VAR175', 'VAR178',
       'POWER']))

    scaling = True
    solar_X, solar_y = data_selection(solar_dataframe1)

    X_train_scaled, X_test_scaled, y_train_scaled, y_test_scaled = LSTM_data_selection(scaling, solar_X, solar_y)

    # print('X_train_scaled: {}\ny_train_scaled: {}\nX_test_scaled: {}\ny_test : {}'.format(X_train_scaled.shape, y_train_scaled.shape, X_test_scaled.shape, y_test_scaled.shape))

    param = {}
    param['num_of_units'] = 250
    param['activation'] = 'relu'
    param['loss'] = 'mae'
    param['optimizer'] = 'adam'
    param['n_epochs'] = 80
    param['batch_size'] = 50

    model = train_LSTM_Dataframe1(X_train_scaled, y_train_scaled, params=param)

    # Model Evaluation
    if scaling == True:
        yhat = model.predict(X_test_scaled)

        test_X = X_test_scaled.reshape((X_test_scaled.shape[0], X_test_scaled.shape[2]))
        # invert scaling for prediction
        inv_yhat = concatenate((yhat, test_X[:, :]), axis=1)
        inv_yhat = scaler.inverse_transform(inv_yhat)
        inv_yhat = inv_yhat[:, 0]
        # invert scaling for actual
        test_y = y_test_scaled.reshape((len(y_test_scaled), 1))
        inv_y = concatenate((test_y, test_X[:, :]), axis=1)
        inv_y = scaler.inverse_transform(inv_y)
        inv_y = inv_y[:, 0]

        print('\n\nMetrics for LSTM Model 1')
        scr = get_metrics_1(y_test_scaled, yhat)
        plot_results_1(inv_y[0:23], inv_yhat[0:23], scr)

    else:
        yhat = model.predict(X_test_scaled)

        print('\n\nMetrics for LSTM Model 1')
        scr = get_metrics_1(y_test_scaled, yhat)
        plot_results_1(y_test_scaled[0:23], yhat[0:23], scr)

    # Save the predicted data result
    #predicted_data = np.column_stack((y_test_scaled.flatten(), yhat.flatten()))
    #np.savetxt('predicted_data_DataFrame1_lstm.dat', predicted_data, delimiter=',')


    #############################################################################################################################
    ''' Solar Sub-dataset 2   '''
    #############################################################################################################################

    solar_dataframe2 = get_dataset(pd.read_csv('processed_data_less_than_avg.csv', sep=',', names=['ZONEID', 'TIMESTAMP', 'VAR157', 'VAR167', 'VAR169', 'VAR175', 'VAR178',
       'POWER']))

    scaling = True
    solar_X, solar_y = data_selection(solar_dataframe2)

    X_train_scaled, X_test_scaled, y_train_scaled, y_test_scaled = LSTM_data_selection(scaling, solar_X, solar_y)


    param = {}
    param['num_of_units'] = 250
    param['activation'] = 'relu'
    param['loss'] = 'mae'
    param['optimizer'] = 'adam'
    param['n_epochs'] = 40
    param['batch_size'] = 300

    model = train_LSTM_Dataframe2(X_train_scaled, y_train_scaled, params=param)

    # Model Evaluation
    if scaling == True:
        yhat = model.predict(X_test_scaled)

        test_X = X_test_scaled.reshape((X_test_scaled.shape[0], X_test_scaled.shape[2]))
        # invert scaling for prediction
        inv_yhat = concatenate((yhat, test_X[:, :]), axis=1)
        inv_yhat = scaler.inverse_transform(inv_yhat)
        inv_yhat = inv_yhat[:, 0]
        # invert scaling for actual
        test_y = y_test_scaled.reshape((len(y_test_scaled), 1))
        inv_y = concatenate((test_y, test_X[:, :]), axis=1)
        inv_y = scaler.inverse_transform(inv_y)
        inv_y = inv_y[:, 0]

        print('\n\nMetrics for LSTM Model 2')
        scr = get_metrics_2(y_test_scaled, yhat)
        plot_results_2(inv_y[0:23], inv_yhat[0:23], scr)

    else:
        yhat = model.predict(X_test_scaled)

        print('\n\nMetrics for LSTM Model 2')
        scr = get_metrics_2(y_test_scaled, yhat)
        plot_results_2(y_test_scaled[0:23], yhat[0:23], scr)

    # Save the predicted data result
    #predicted_data = np.column_stack((y_test_scaled.flatten(), yhat.flatten()))
    #np.savetxt('predicted_data_DataFrame2_lstm.dat', predicted_data, delimiter=',')



    #############################################################################################################################
    ''' Solar Sub-dataset 3   '''
    #############################################################################################################################

    solar_dataframe3 = get_dataset(pd.read_csv('processed_data_less_VAR175_greater_VAR178.csv', sep=',', names=['ZONEID', 'TIMESTAMP', 'VAR157', 'VAR167', 'VAR169', 'VAR175', 'VAR178',
       'POWER']))

    scaling = True
    solar_X, solar_y = data_selection(solar_dataframe3)

    X_train_scaled, X_test_scaled, y_train_scaled, y_test_scaled = LSTM_data_selection(scaling, solar_X, solar_y)


    param = {}
    param['num_of_units'] = 150
    param['activation'] = 'relu'
    param['loss'] = 'mae'
    param['optimizer'] = 'adam'
    param['n_epochs'] = 50
    param['batch_size'] = 50


    model = train_LSTM_Dataframe3(X_train_scaled, y_train_scaled, params=param)

    # Model Evaluation
    if scaling == True:
        yhat = model.predict(X_test_scaled)

        test_X = X_test_scaled.reshape((X_test_scaled.shape[0], X_test_scaled.shape[2]))
        # invert scaling for prediction
        inv_yhat = concatenate((yhat, test_X[:, :]), axis=1)
        inv_yhat = scaler.inverse_transform(inv_yhat)
        inv_yhat = inv_yhat[:, 0]
        # invert scaling for actual
        test_y = y_test_scaled.reshape((len(y_test_scaled), 1))
        inv_y = concatenate((test_y, test_X[:, :]), axis=1)
        inv_y = scaler.inverse_transform(inv_y)
        inv_y = inv_y[:, 0]

        print('\n\nMetrics for LSTM Model 3')
        scr = get_metrics_3(y_test_scaled, yhat)
        plot_results_3(inv_y[0:23], inv_yhat[0:23], scr)

    else:
        yhat = model.predict(X_test_scaled)

        print('\n\nMetrics for LSTM Model 3')
        scr = get_metrics_3(y_test_scaled, yhat)
        plot_results_3(y_test_scaled[0:23], yhat[0:23], scr)

    # Save the predicted data result
    predicted_data = np.column_stack((y_test_scaled.flatten(), yhat.flatten()))
    np.savetxt('predicted_data_DataFrame3_lstm.dat', predicted_data, delimiter=',')


    #############################################################################################################################
    ''' Solar Sub-dataset 4   '''
    #############################################################################################################################

    solar_dataframe4 = get_dataset(pd.read_csv('processed_data_greater_VAR178_less_VAR175.csv', sep=',', names=['ZONEID', 'TIMESTAMP', 'VAR157', 'VAR167', 'VAR169', 'VAR175', 'VAR178',
       'POWER']))

    scaling = True
    solar_X, solar_y = data_selection(solar_dataframe4)

    X_train_scaled, X_test_scaled, y_train_scaled, y_test_scaled = LSTM_data_selection(scaling, solar_X, solar_y)


    param = {}
    param['num_of_units'] = 150
    param['activation'] = 'relu'
    param['loss'] = 'mse'
    param['optimizer'] = 'adam'
    param['n_epochs'] = 30
    param['batch_size'] = 400

    model = train_LSTM_Dataframe4(X_train_scaled, y_train_scaled, params=param)

    # Model Evaluation
    if scaling == True:
        yhat = model.predict(X_test_scaled)

        test_X = X_test_scaled.reshape((X_test_scaled.shape[0], X_test_scaled.shape[2]))
        # invert scaling for prediction
        inv_yhat = concatenate((yhat, test_X[:, :]), axis=1)
        inv_yhat = scaler.inverse_transform(inv_yhat)
        inv_yhat = inv_yhat[:, 0]
        # invert scaling for actual
        test_y = y_test_scaled.reshape((len(y_test_scaled), 1))
        inv_y = concatenate((test_y, test_X[:, :]), axis=1)
        inv_y = scaler.inverse_transform(inv_y)
        inv_y = inv_y[:, 0]

        print('\n\nMetrics for LSTM Model 4')
        scr = get_metrics_4(y_test_scaled, yhat)
        plot_results_4(inv_y[0:23], inv_yhat[0:23], scr)

    else:
        yhat = model.predict(X_test_scaled)

        print('\n\nMetrics for LSTM Model 4')
        scr = get_metrics_4(y_test_scaled, yhat)
        plot_results_4(y_test_scaled[0:23], yhat[0:23], scr)

    # Save the predicted data result
    #predicted_data = np.column_stack((y_test_scaled.flatten(), yhat.flatten()))
    #np.savetxt('predicted_data_DataFrame4_lstm.dat', predicted_data, delimiter=',')



if __name__ == '__main__':
    main()