import matplotlib.image as image
import matplotlib.pyplot as plt
# Set the font dictionaries (for plot title and axis titles)
title_font = {'fontname': 'Arial', 'size': '20', 'color': 'black', 'weight': 'normal',
              'verticalalignment': 'bottom'}  # Bottom vertical alignment for more space
axis_font = {'fontname': 'Arial', 'size': '18'}



# ANN result evaluation
plt.subplot(221)
img = image.imread('Experimenmt_Result/ANN/dataframe_1_ann.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 1')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(222)
img = image.imread('Experimenmt_Result/ANN/dataframe_2_ann.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 2')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(223)
img = image.imread('Experimenmt_Result/ANN/dataframe_3_ann.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 3')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

plt.subplot(224)
img = image.imread('Experimenmt_Result/ANN/dataframe_4_ann.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 4')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis



# LSTM result evaluation
plt.subplot(221)
img = image.imread('Experimenmt_Result/LSTM/dataframe_1_lstm.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 1')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(222)
img = image.imread('Experimenmt_Result/LSTM/dataframe_2_lstm.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 2')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(223)
img = image.imread('Experimenmt_Result/LSTM/dataframe_3_lstm.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 3')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

plt.subplot(224)
img = image.imread('Experimenmt_Result/LSTM/dataframe_4_lstm.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 4')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis



# KNN result evaluation
plt.subplot(221)
img = image.imread('Experimenmt_Result/KNN/dataframe_1_knn.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 1')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(222)
img = image.imread('Experimenmt_Result/KNN/dataframe_2_knn.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 2')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(223)
img = image.imread('Experimenmt_Result/KNN/dataframe_3_knn.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 3')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

plt.subplot(224)
img = image.imread('Experimenmt_Result/KNN/dataframe_4_knn.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 4')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


# Gradient Boosting Regressor result evaluation
plt.subplot(221)
img = image.imread('Experimenmt_Result/GBR/dataframe_1_gbr.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 1')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(222)
img = image.imread('Experimenmt_Result/GBR/dataframe_2_gbr.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 2')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(223)
img = image.imread('Experimenmt_Result/GBR/dataframe_3_gbr.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 3')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

plt.subplot(224)
img = image.imread('Experimenmt_Result/GBR/dataframe_4_gbr.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 4')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


# Decission Tree Regressor result evaluation
plt.subplot(221)
img = image.imread('Experimenmt_Result/DTR/dataframe_1_dtr.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 1')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(222)
img = image.imread('Experimenmt_Result/DTR/dataframe_2_dtr.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 2')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(223)
img = image.imread('Experimenmt_Result/DTR/dataframe_3_dtr.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 3')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

plt.subplot(224)
img = image.imread('Experimenmt_Result/DTR/dataframe_4_dtr.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 4')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis



# Support Vector Regression result evaluation
plt.subplot(221)
img = image.imread('Experimenmt_Result/SVR/dataframe_1_svr.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 1')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(222)
img = image.imread('Experimenmt_Result/SVR/dataframe_2_svr.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 2')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.subplot(223)
img = image.imread('Experimenmt_Result/SVR/dataframe_3_svr.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 3')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis

plt.subplot(224)
img = image.imread('Experimenmt_Result/SVR/dataframe_4_svr.png',0)
plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.title('Solar Dataframe 4')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis


plt.show()
