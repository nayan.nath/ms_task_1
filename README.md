# Solar Power Energy Forecasting (RWTH Aachen Master Thesis Part 1)

The expanding interest for energy is one of the main motivation behind the integration of solar energy into the electric grids or networks. The exact expectation of solar oriented irradiance variety can improve the nature of administration. This coordination of
solar based vitality and exact expectation can help in better arranging and distribution of energy. Discovering vitality sources to fulfill the world’s developing interest is one of the general public’s first difficulties for the following 50 years. Two machine learning technique are utilized for solar power forecasting. Normalized the experimental data, choosing best features selection method and removing redundant information from raw data, split datasets on four different weather profiles and right choice for time series machine learning algorithm made solar power prediction model more robust and efficient for solar power energy forecasting.

Two different novels data mining and machine learning techniques, artificial neural network (ANN) and long short time memory (LSTM) has been used to conduct the work.

| File | Description |
| ------ | ------ |
| dataset_processing.py | Calculate the mutual info regression for all solar data features |
| data_generation.py | Generate four seperate DataFrame based on mutual info regression score with condition | 
| main_ANN.py | Build ANN model for four different data frame | 
| main_LSTM.py | Build LSTM model for four different data frame | 
| result_evaluation.py | All the necessary plot for solar predicted result |


